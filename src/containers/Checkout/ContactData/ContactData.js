import React, { Component } from 'react'
import Button from '../../../components/UI/Button/Button'
import Spinner from '../../../components/UI/Spinner/Spinner'

import classes from './ContactData.module.css'
import axios from '../../../axios-orders'

class ContactData extends Component {
	state = {
		name: '',
		email: '',
		address: {
			street: '',
			postal: ''
		},
		loading: false
	}
	orderHandler = (event) => {
		event.preventDefault()
		this.setState({ loading: true })
		const order = {
			ingredients: this.props.ingredients,
			price: this.props.price,
			customer: {
				name: 'Rui',
				address: {
					street: '122 th st NE',
					zip: '98052'
				},
				email: 'testRui@test.com'
			},
			deliveryMethods: 'Plane'
		}
		axios.post('/orders.json', order)
		.then(res => {
			this.setState({ loading: false })
			this.props.history.push('/')
		})
		.catch(err => {
			this.setState({ loading: false })
		})
	}
	render () {
		let form = (
			<form>
				<input className={classes.Input} type="text" name="name" placeholder="Your Name" />
				<input className={classes.Input} type="email" name="email" placeholder="Your Email" />
				<input className={classes.Input} type="text" name="street" placeholder="Street" />
				<input className={classes.Input} type="text" name="postal" placeholder="Postal" />
				<Button btnType="Success" clicked={this.orderHandler}>ORDER</Button>
			</form>
		)
		if (this.state.loading) {
			form = <Spinner />
		}
		return (
			<div className={classes.ContactData}>
				<h4>Enter your Contact Data</h4>
				{ form }
			</div>
		)
	}

}
export default ContactData